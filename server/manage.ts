import { getRepository } from "typeorm";

import { User } from "./db";
import * as ServerTypes from "./server-types";

export async function Manage(ctx: ServerTypes.EvalLoopContext, response?: ServerTypes.RouteHandlerResponse) {
    const users = await getRepository(User).find();
    return { Users: users };
}
