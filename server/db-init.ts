import { createConnection, ConnectionOptions } from "typeorm";
import * as Models from "./db";
import { Settings } from "./server-types";

export function runInitPatch() {
    const conf: Settings = require("./settings.json");
    const defaultConnection = conf.databaseOptions[0];
    defaultConnection.entities = Object.keys(Models).map((key) => { return (Models as any)[key]; });

    createConnection(conf.databaseOptions[1] as ConnectionOptions)
        .then((rootConnection) =>
            Promise.all([
                // create database
                rootConnection.query(`CREATE DATABASE IF NOT EXISTS \`${defaultConnection.database}\`;`),
                // create user
                rootConnection.query(`CREATE USER IF NOT EXISTS ${defaultConnection.username}@localhost IDENTIFIED BY '${defaultConnection.password}';`),
                rootConnection.query(`GRANT ALL ON \`${defaultConnection.database}\`.* TO ${defaultConnection.username} IDENTIFIED BY '${defaultConnection.password}';`)
            ])
                .then(() => {
                    rootConnection.close();
                    console.log("Init patch succefully applied");
                })
                .catch((err) => console.log(err))
                .then(null)
        )
        .catch((err) => console.log(err))
        .then(() => {
            // create tables
            createConnection(defaultConnection as ConnectionOptions)
                .then(async (conn) => {
                    const mainUser = new Models.User(conf.email, true);
                    await conn.manager.save(mainUser);

                    conn.close();
                    console.log("Create table patch applied");
                })
                .catch((err) => console.log(err))
                .then(null);
        })
        .then(null);
}
