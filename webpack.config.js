module.exports = [{
    mode: 'production',
    entry: {
        write: __dirname + "/client/write.js",
        render: __dirname + "/client/render.js",
        join: __dirname + "/client/join.js",
    },
    output: {
        path: __dirname + "/static/bundle",
        filename: "[name].js"
    },
    resolve: {
        extensions: [".js"]
    },
    performance: {
        hints: false
    }
},
{
    mode: 'development',
    entry: __dirname + "/client/manage.ts",
    output: {
        path: __dirname + "/static/bundle",
        filename: "manage.js"
    },
    resolve: {
        extensions: [".tsx", ".ts", ".js"],
        alias: {
            'vue$': 'vue/dist/vue.esm.js',
        }
    },
    module: {
        rules: [
            { test: /\.tsx?$/, loader: "ts-loader" }
        ]
    },
    performance: {
        hints: false
    }
}];