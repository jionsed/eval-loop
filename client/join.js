import * as $ from 'jquery';
import * as Noty from 'noty';

function info(text, type) {
    new Noty({ text: text, theme: "relax", type: type }).show();
}

const email = document.getElementById("email");
const emailForm = document.getElementById("email-form");

const code = document.getElementById("onetime-code");
const oneTimeCodeForm = document.getElementById("onetime-code-form");

document.getElementById("email-enter").addEventListener("click", () => {
    $.ajax(`/v1/session/new?email=${email.value}`, {
        method: "GET"
    }).done(() => {
        info("Login key sended to email", "success");

        emailForm.classList.toggle("hidden");
        oneTimeCodeForm.classList.toggle("hidden");
    }).fail((data) => {
        info(data.responseJSON.message, "warning");
    });
});

document.getElementById("onetime-code-enter").addEventListener("click", () => {
    $.get(`/v1/session/login?key=${code.value}`).done((params) => {
        info("Login succefull, you will be redirected after 2 seconds", "success");
        setTimeout(() => window.location.href = "/", 2000);
    }).fail((data) => {
        info(data.responseText, "warning");
    }).always(() => { });
});
