import * as $ from 'jquery';
import _ from "underscore";

function removePost() {
    $.get("/v1/delete/post/" + this.dataset["id"]).then(() => {
        if (window.location.href.indexOf("post") > 0)
            window.location.href = "/";
        else
            location.reload();
    });
}

_.each(document.getElementsByClassName("icon-cross"), (item) => {
    item.addEventListener("click", removePost.bind(item));
});
