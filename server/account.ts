import * as nodemailer from "nodemailer";
import * as ORM from "typeorm";

import { User, Session } from "./db";
import { Point, EvalLoopContext, RouteHandlerResponse } from "./server-types";

async function loginCb(ctx: EvalLoopContext, response?: RouteHandlerResponse) {
    const user = await ORM.getRepository(User).createQueryBuilder("user")
        .leftJoinAndSelect("user.sessions", "session")
        .where("user.email = :email", { email: ctx.query.email }).getOne();

    if (!user)
        return response!(404, { message: "Email not found" });

    var session = new Session();
    session.user = user;
    ORM.getRepository(Session).save(session);

    const transport = nodemailer.createTransport({
        service: "gmail",
        auth: {
            user: ctx.config.email,
            pass: ctx.config.emailPassword
        }
    });
    transport.sendMail({
        from: ctx.config.email,
        to: ctx.query.email,
        subject: "Eval-loop: editor access autorization",
        html: `You login code is ${session.key}`
    }, () => { });

    return response!(200, session);
}

async function authCb(ctx: EvalLoopContext, response?: RouteHandlerResponse) {
    const session = await ORM.getRepository(Session).createQueryBuilder("session")
        .select().where("session.key = :key", { key: ctx.query.key }).getOne();
    if (!session)
        return response!(403, { message: "Access denied" });

    ctx.session!.ukey = session.key;
    ctx.session!.save();

    if (session.active)
        return response!(200, session);

    session.active = true
    ORM.createQueryBuilder("session").update(session);
    return response!(200, session);
}

async function getUserCb(ctx: EvalLoopContext, response?: RouteHandlerResponse) {
    const user = await ORM.getRepository(User).createQueryBuilder("user")
        .select().where("user.id = :id", { id: ctx.params.id }).getOne();
    if (user)
        return response!(200, user);
    else
        return response!(404, { message: "User not found" })
}

async function getUsersCb(ctx: EvalLoopContext, response?: RouteHandlerResponse) {
    return response!(200, await ORM.getRepository(User).find());
}

async function saveUserCb(ctx: EvalLoopContext, response?: RouteHandlerResponse) {
    const postForm = (ctx.request as any).body;

    console.log(postForm);
    const user = await ORM.getRepository(User).createQueryBuilder("user")
        .select().where("user.email = :email", { email: postForm.email }).getOne();
    if (user)
        return response!(403, { message: "Email already exists" });

    const result = await ORM.getRepository(User).save(new User(postForm.email));
    return response!(200, result);
}

export const Login = new Point("/v1/session/new", true, loginCb);
export const Authorize = new Point("/v1/session/login", true, authCb);
export const GetUser = new Point("/v1/get/users/:id", false, getUserCb);
export const GetUsers = new Point("/v1/get/users", false, getUsersCb);
export const SaveUser = new Point("/v1/get/users", false, saveUserCb, "POST")