import { getRepository } from "typeorm";
import * as uuidv4 from "uuid/v4";

import { Post } from "./db";
import * as ServerTypes from "./server-types";

async function savePostCb(ctx: ServerTypes.EvalLoopContext, response?: ServerTypes.RouteHandlerResponse) {
    const postData = (ctx.request as any).body;
    const uid = (ctx.query.uid) ? ctx.query.uid : postData.uid;
    var post = await getRepository(Post).createQueryBuilder("post")
        .select().where("post.uid = :uid", { uid: uid }).getOne();

    if (!post)
        post = new Post();

    ServerTypes.fromQuery(post, postData);
    await getRepository(Post).save(post);

    return response!(200, post);
}

async function deletePostCb(ctx: ServerTypes.EvalLoopContext, response?: ServerTypes.RouteHandlerResponse) {
    const result = await getRepository(Post).createQueryBuilder("post").delete()
        .where("post.uid = :uid", { uid: ctx.params.uid }).execute();
    return response!(200, { ok: result.raw.fieldCount == 1 });
}

// ### view method
export async function GetAll(ctx: ServerTypes.EvalLoopContext, response?: ServerTypes.RouteHandlerResponse) {
    const posts = await getRepository(Post).createQueryBuilder("post").select().getMany();
    if (posts.length > 0)
        return { Posts: posts };
    else
        return {};
}

// ### view method
export async function GetOne(ctx: ServerTypes.EvalLoopContext, response?: ServerTypes.RouteHandlerResponse) {
    const post = await getRepository(Post).createQueryBuilder("post")
        .select().where("post.uid = :uid", { uid: ctx.params.uid }).getOne();
    return { Post: post };
}

export const Save = new ServerTypes.Point("/v1/save/post", false, savePostCb, "POST");
export const Delete = new ServerTypes.Point("/v1/delete/post/:uid", false, deletePostCb, "GET");