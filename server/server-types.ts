import * as Koa from 'koa';
import * as koaSession from 'koa-session';
import * as Router from 'koa-router';

import { User, Post as PostModel } from "./db";
import { JL } from 'jsnlog';
import { Dictionary } from 'underscore';

export type ConnecionSettings = {
    name: string;
    type: string;
    host: string;
    port: number;
    username: string;
    password: string;
    database: string;
    entities: any[];
};

export interface Settings {
    email: string;
    subject: string;
    emailPassword: string;
    port: number;
    databaseURL: string;
    databaseOptions: ConnecionSettings[];
    sessionKey: string[];
};

export interface PageTemplateData {
    AccessAllowed: boolean;
    Post?: PostModel;
    Posts?: PostModel[];
    Users?: User[];
}

export interface EvalLoopContext extends Koa.Context {
    config: Settings;
    logger: JL.JSNLogLogger;
};

export type RouteHandlerResponse = (status: number, response: any) => void;
export type RouteHandler = (ctx: EvalLoopContext, response?: RouteHandlerResponse) => any;

export function fromQuery(model: any, query: any): void {
    // TODO
    for (const key in query)
        if (query[key])
            model[key] = query[key];
}

export abstract class Route {
    path: string;
    routeHandler?: RouteHandler;
    method: string = "get";
    isPublic: boolean;

    constructor(path: string, handler?: RouteHandler, isPublic: boolean = true) {
        this.path = path;
        this.routeHandler = handler;
        this.isPublic = isPublic;
    }

    abstract render(): Router.IMiddleware;

    accessAllowed(context: EvalLoopContext): boolean {
        if (this.isPublic)
            return true;

        return !!(context.session!.ukey);
    }

    register(router: Router) {
        router.register(this.path, [this.method], this.render());
    }
}

function ok(this: EvalLoopContext, status: number, response: any) {
    this.response.status = status;
    this.response.body = response;
}

export class Point extends Route {
    method: string;

    constructor(path: string, isPublic: boolean = false, handler: RouteHandler, method: string = "get") {
        super(path, handler, isPublic);
        this.method = method;
    }

    render(): Router.IMiddleware {
        return async (ctx: any) => {
            if (!this.accessAllowed(ctx)) {
                ctx.logger.info(`403:${ctx.url}: Access denied`);
                ok.call(ctx, 403, { message: "Access denied" });
            } else {
                await this.routeHandler!(ctx, ok.bind(ctx));
            }
        };
    }
}

function isJSON(headers: any): boolean {
    return headers["content-type"] && (headers["content-type"].indexOf("json") > 0);
}

export abstract class EvalLoop extends Koa {
    abstract config: Settings;
    abstract router: Router;
    abstract logger: JL.JSNLogLogger;

    run() {
        this.listen(this.config.port, () => {
            this.logger.info(`Server running on port ${this.config.port}`);
        });
        return this;
    }

    async onErrorHandler(ctx: EvalLoopContext, next: any) {
        try {
            await next();
            ctx.logger.info(`${ctx.status}:${ctx.method}:${ctx.url}`);

            if (ctx.status != 200)
                ctx.throw(ctx.status);
        } catch (err) {
            if (ctx.status != 404)
                ctx.logger.error(err);

            if (isJSON(ctx.request.headers) || isJSON(ctx.response.headers))
                ok.bind(ctx, ctx.status, ctx.body);
            else
                await ctx.render("error",
                    {
                        status: ctx.status,
                        message: err.message,
                        AccessAllowed: !!ctx.session!.ukey
                    });
        }
    }
}
