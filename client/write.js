import * as $ from 'jquery';
import * as Noty from 'noty';
import tinymce from 'tinymce/tinymce';

tinyMCE.PluginManager.add('stylebuttons', function (editor, url) {
    ['pre', 'p', 'code', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6'].forEach(function (name) {
        editor.addButton("style-" + name, {
            tooltip: "Toggle " + name,
            text: name.toUpperCase(),
            onClick: function () { editor.execCommand('mceToggleFormat', false, name); },
            onPostRender: function () {
                var self = this, setup = function () {
                    editor.formatter.formatChanged(name, function (state) {
                        self.active(state);
                    });
                };
                editor.formatter ? setup() : editor.on('init', setup);
            }
        })
    });
});

const post = {
    uid: document.getElementById("live-editor").dataset.postId,
    title: document.getElementById("post-title-editable"),
    content: document.getElementById("editor-container"),
    save: document.getElementById("save-post"),
    status: document.getElementById("post-status")
};

const toolbar = `undo redo styleselect bold italic style-h1 style-h2 style-h3 style-h4 style-h5 
toc alignleft aligncenter alignright bullist numlist outdent indent link image code`;
const editor = tinymce.init({
    selector: "#editor-container",
    menubar: false,
    toolbar: toolbar,
    plugins: 'code toc link image stylebuttons',
    min_height: 700
});

post.save.addEventListener("click", () => {
    $.ajax({
        url: "/v1/save/post",
        type: "POST",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: JSON.stringify({
            title: post.title.value,
            content: tinymce.activeEditor.getContent(),
            visible: !post.status.classList.contains("icon-eye-hidden"),
            uid: post.uid
        })
    }).then((data) => {
        new Noty({ text: "Post saved" }).show();

        if (post.uid !== data.uid)
            setTimeout(() => window.location.href = "/write/" + data.uid, 2000);
    }).catch(() => {
        new Noty({ text: "Oops, somethings happening here, try again later" }).show();
    });
});

post.status.addEventListener("click", () => {
    if (post.status.classList.contains("icon-eye-hidden")) {
        post.status.classList.remove("icon-eye-hidden");
        post.status.classList.add("icon-eye-visible");
        return;
    }

    if (post.status.classList.contains("icon-eye-visible")) {
        post.status.classList.remove("icon-eye-visible");
        post.status.classList.add("icon-eye-hidden");
    }
});
