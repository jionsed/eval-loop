import axios, { AxiosResponse, AxiosError } from 'axios';
import * as Noty from "noty";
import Vue from "vue";

interface User {
    id: number;
    email: string;
}

interface UsersListModel {
    users: User[];
    showUserForm: boolean;
    email: string;
}

class API {
    static request<T>(method: Function, args: any[]): Promise<T> {
        return new Promise<T>((resolve) => {
            method.apply(null, args)
                .then((response: AxiosResponse) => resolve(response.data))
                .catch((err: AxiosError) => {
                    if (err.response && err.response!.data)
                        new Noty({
                            text: err.response!.data.message,
                            theme: "relax",
                            type: "warning"
                        }).show();
                    resolve(undefined);
                });
        });
    }

    static users(): Promise<User[]> {
        return API.request<User[]>(axios.get, ["/v1/get/users"]);
    }

    static newUser(email: string) {
        return API.request<User>(axios.post, ["/v1/get/users", { email: email }]);
    }
}

const UsersListView = Vue.component("users-list", {
    template: "#users-list-template",
    data(): UsersListModel {
        return {
            users: [],
            showUserForm: true,
            email: ""
        };
    },
    created() {
        API.users()
            .then((data: any) => this.users = data);
    },
    methods: {
        showForm() {
            this.showUserForm = !this.showUserForm;
        },
        addUser() {
            API.newUser(this.email)
                .then(_ => API.users())
                .then(data => this.users = data);
        }
    }
})

const App = new Vue({
    el: "#page",
    components: {
        UsersListView
    }
});
