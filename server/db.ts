import { Entity, PrimaryGeneratedColumn, Column, OneToMany, ManyToOne } from "typeorm";
import * as uuidv4 from "uuid/v4";

@Entity()
export class User {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    email: string;

    @Column()
    root: boolean;

    @OneToMany(type => Session, session => session.user)
    sessions: Session[] | null;

    constructor(email: string = "", isRoot: boolean = false) {
        this.id = 0;
        this.email = email;
        this.sessions = null;
        this.root = isRoot;
    }
}

@Entity()
export class Session {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    key: string;

    @Column()
    active: boolean;

    @Column()
    creationTime: Date;

    @Column()
    lifeTime: Date;

    @ManyToOne(type => User, user => user.sessions)
    user: User | null;

    constructor() {
        this.id = 0;
        this.key = "";
        this.key = uuidv4();
        this.active = false;
        this.creationTime = new Date();
        this.lifeTime = new Date();
        this.lifeTime.setFullYear(this.lifeTime.getFullYear() + 5);
        this.user = null;
    }
}

@Entity()
export class Post {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    uid: string;

    @Column()
    title: string;

    @Column()
    content: string;

    @Column()
    visible: boolean;

    @Column()
    creationTime: Date;

    constructor() {
        this.id = 0;
        this.uid = uuidv4();
        this.creationTime = new Date();
        this.title = "";
        this.content = "";
        this.visible = false;
    }
}