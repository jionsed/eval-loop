import "reflect-metadata";

import { JL } from 'jsnlog';
import * as bodyParser from "koa-body";
import { Context } from "koa";
import { Middleware } from "koa-compose";
import * as Router from 'koa-router';
import * as session from "koa-session";
import * as serve from "koa-static";
import * as views from "koa-views";
import * as path from "path";
import * as ORM from "typeorm";

import * as account from "./account";
import * as posts from "./posts";
import { User, Session, Post as PostModel } from "./db";
import * as ServerTypes from "./server-types";
import { Manage } from "./manage";

class Page extends ServerTypes.Route {
    template: string;

    constructor(path: string, template: string, isPublic: boolean = true, handler?: ServerTypes.RouteHandler) {
        super(path, handler, isPublic);
        this.template = template;
    }

    render(): Router.IMiddleware {
        return async (ctx: any, next) => {
            if (!this.isPublic && !ctx.session.ukey) {
                ctx.logger.info(`403:${ctx.url}: Access denied`);
                return await ctx.render("error", { status: 404, message: "Not Found" });
            }

            const data = Object.assign(
                { accessAllowed: !!ctx.session.ukey },
                (this.routeHandler) ? await this.routeHandler(ctx) : {});
            await ctx.render(this.template, data);
        }
    }
}

export class App extends ServerTypes.EvalLoop {
    config: ServerTypes.Settings;
    router: Router;
    logger: JL.JSNLogLogger;

    static API: any = {
        Static: {
            Index: {
                Main: new Page("/", "index", true, posts.GetAll),
                Join: new Page("/join", "join"),
                Manage: new Page("/view-all", "manage", false, Manage)
            },
            Post: {
                Write: new Page("/write", "write", false),
                Edit: new Page("/write/:uid", "write", false, posts.GetOne),
                Read: new Page("/post/:uid", "post", true, posts.GetOne)
            },
        },
        REST: {
            Post: {
                Save: posts.Save,
                Delete: posts.Delete
            },
            Account: {
                Login: account.Login,
                Auth: account.Authorize,
                User: account.GetUser,
                Users: account.GetUsers,
                New: account.SaveUser
            }
        }
    };
    connection: ORM.Connection | null;

    constructor() {
        super();
        this.logger = JL();

        this.router = new Router();
        this.initAPI(App.API);
        this.config = require("./settings.json");
        this.config.databaseOptions[0].entities = [User, PostModel, Session];
        (this.context as ServerTypes.EvalLoopContext).config = this.config;
        (this.context as ServerTypes.EvalLoopContext).logger = this.logger;
        this.connection = null;
        ORM.createConnection(this.config.databaseOptions[0] as ORM.ConnectionOptions)
            .then((conn) => this.connection = conn);

        this.keys = this.config.sessionKey;
        this.use(this.onErrorHandler as Middleware<Context>);
        this.use(bodyParser());
        this.use(session({ key: "session", signed: false, maxAge: 86400000 }, this));

        this.use(views(path.join(__dirname, '/views'), { map: { html: 'nunjucks' } }));
        this.use(this.router.routes());
        this.use(this.router.allowedMethods());

        this.use(serve(path.join(__dirname, '/../static')));
        this.use(serve(path.join(__dirname, '/../node_modules')));
    }

    initAPI(obj: any) {
        for (const key in obj)
            if (obj[key].register)
                obj[key].register(this.router);
            else
                this.initAPI(obj[key]);
    }
}
