# Eval-loop is my personal node.js based hello-world

## Main dependecies

git, nodejs, mariadb

## Latest node version (deb) with dev deps

curl -sL https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list
sudo apt-get update && sudo apt-get install yarn

curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -
sudo apt-get install -y nodejs
npm install -g webpack webpack-cli ts-node typescript

## Config file

cp ./server/settings.sample.json ./server/settings.json

## Building

npm install
webpack-cli
cp -rf node_modules/tinymce/themes static/bundle/
cp -rf node_modules/tinymce/skins static/bundle/
cp -rf node_modules/tinymce/plugins static/bundle/
